package com.hm.springcourse.view;


import com.hm.springcourse.model.Ticket;
import com.lowagie.text.Table;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class TicketPdfView extends AbstractPdfView {
    @Override
    protected void buildPdfDocument(Map<String, Object> map, com.lowagie.text.Document document, com.lowagie.text.pdf.PdfWriter pdfWriter, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        List<Ticket> tickets = (List<Ticket>)map.get("tickets");
        Table table = new Table(6);
        table.addCell("Id");
        table.addCell("Event Name");
        table.addCell("User Email");
        table.addCell("Ticket Created Date");
        table.addCell("Seats");
        table.addCell("Price");

        for (Ticket ticket : tickets) {
            table.addCell(String.valueOf(ticket.getId()));
            table.addCell(ticket.getEvent().getName());
            table.addCell(ticket.getUser().getEmail());
            table.addCell(ticket.getDateTime().toString());
            table.addCell(ticket.getSeats());
            table.addCell(String.valueOf(ticket.getPrice()));
        }
        document.add(table);
    }
}
