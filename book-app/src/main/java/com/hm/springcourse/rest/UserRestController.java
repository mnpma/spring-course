package com.hm.springcourse.rest;

import com.hm.springcourse.model.User;
import com.hm.springcourse.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
public class UserRestController {
    @Autowired
    private UserService userService;

    @GetMapping("/restUserList")
    ResponseEntity<List<User>> getUserList() {
        List<User> users = userService.findAll();
        return users == null ? ResponseEntity.noContent().build() : ResponseEntity.ok(users);
    }

}
