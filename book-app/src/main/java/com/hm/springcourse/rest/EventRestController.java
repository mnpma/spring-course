package com.hm.springcourse.rest;

import com.hm.springcourse.model.Event;
import com.hm.springcourse.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
public class EventRestController {
    @Autowired
    private EventService eventService;

    @GetMapping("/restEventList")
    public ResponseEntity<List<Event>> getEventList() {
        List<Event> events = eventService.getAll();
        return events == null ? ResponseEntity.noContent().build() : ResponseEntity.ok(events);
    }
}
