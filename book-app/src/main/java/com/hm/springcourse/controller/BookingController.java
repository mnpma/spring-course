package com.hm.springcourse.controller;


import com.hm.springcourse.dto.BookingTicketDto;
import com.hm.springcourse.dto.TicketFilterDto;
import com.hm.springcourse.model.Auditorium;
import com.hm.springcourse.model.Event;
import com.hm.springcourse.model.Ticket;
import com.hm.springcourse.model.User;
import com.hm.springcourse.service.AuditoriumService;
import com.hm.springcourse.service.BookingService;
import com.hm.springcourse.service.EventService;
import com.hm.springcourse.service.UserService;
import com.hm.springcourse.util.CsvUtil;
import com.hm.springcourse.view.TicketPdfView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class BookingController {
    @Autowired
    private BookingService bookingService;
    @Autowired
    private UserService userService;
    @Autowired
    private EventService eventService;
    @Autowired
    private AuditoriumService auditoriumService;
    @RequestMapping(value = "/bookingTicket", method = RequestMethod.GET)
    public String getParamsToBookingTicket(Model model, @RequestParam Long userId) {

        List<String> emails = userService.getUserEmailListDto();
        if (!model.containsAttribute("bookingTicketDto")) {
            BookingTicketDto bookingTicketDto = new BookingTicketDto();
            //bookingTicketDto.setUserDto(userMapDto.get(userId));
            bookingTicketDto.setEmail("user.test@gmail.com");
            bookingTicketDto.setTicketPrice(String.valueOf(0.0));
            bookingTicketDto.setEventDateTime(LocalDateTime.of(2011, Month.APRIL, 20, 10, 10));
            bookingTicketDto.setEventName("Event1");
            bookingTicketDto.setAuditoriumName("Blue hall");
            bookingTicketDto.setSeats("1,2,3");
            model.addAttribute("bookingTicketDto", bookingTicketDto);
        }
        //model.addAttribute("userMapDto", userMapDto);
        model.addAttribute("userMapDto", emails);
        return "bookingTicket";
    }

    @RequestMapping(value = "/bookingTicket", method = RequestMethod.POST)
    public String bookingTicketByEventForAuditorium(Model model,
                                                    @ModelAttribute("bookingTicketDto") BookingTicketDto bookingTicketDto) {
        String userEmail = bookingTicketDto.getEmail();
        User user = userService.getUserByEmail(bookingTicketDto.getEmail());
        List<Integer> seats = CsvUtil.fromCsvToList(bookingTicketDto.getSeats(), Integer::valueOf);
        Ticket storedTicket = bookingService.getTicketWithPrice(bookingTicketDto.getEventName(),
                bookingTicketDto.getAuditoriumName(), bookingTicketDto.getEventDateTime(),
                seats, /*user.getId()*/1L);
        return "redirect:/userListSummary";
    }


    @GetMapping("/ticketFilter")
    public String viewTicketFilter(Model model) {
        if(!model.containsAttribute("ticketFilterDto")) {
            TicketFilterDto ticketFilterDto = new TicketFilterDto("Event1", "Blue hall");
            model.addAttribute("ticketFilterDto", ticketFilterDto);
        }
        List<String> eventNameList = eventService.getAll().stream().map(Event::getName).collect(Collectors.toList());
        List<String> auditoriumNameList = auditoriumService.getAll().stream().map(Auditorium::getName).collect(Collectors.toList());
        model.addAttribute("eventNameList", eventNameList);
        model.addAttribute("auditoriumNameList", auditoriumNameList);
        model.addAttribute("reportTypeList", Arrays.asList("pdf", "form"));
        return "ticketFilter";
    }

    @PostMapping("/ticketFilter")
    public String postTicketFilter(Model model,
                                   @ModelAttribute("ticketFilterDto")TicketFilterDto ticketFilterDto) {
        String eventName = ticketFilterDto.getEventName();
        String auditoriumName = ticketFilterDto.getAuditoriumName();
        String reportType = ticketFilterDto.getReportType();
        if("pdf".equals(reportType)) {
            return "redirect:/bookingTicket/list/pdf/"+eventName+"/"+auditoriumName;
        }
        return "redirect:/bookingTicket/list/"+eventName+"/"+auditoriumName;
    }

    @RequestMapping(value="/bookingTicket/list/{eventName}/{auditoriumName}", method = RequestMethod.GET)
    public String bookingTicketList(Model model, @PathVariable String eventName, @PathVariable String auditoriumName) {
        List<Ticket> tickets = bookingService.getTicketsForEvent(eventName, auditoriumName,
                LocalDateTime.of(2011, Month.APRIL, 20, 10, 10));
        model.addAttribute("tickets", tickets);
        return "ticketList";
    }

/*
    For operations, that return list of booked tickets (by event, or by user)
    implement alternative controllers, that will return result as PDF document.
    Map this controller to a specific value of Accept request header  - Accept=application/pdf
*/
    @RequestMapping(value="/bookingTicket/list/pdf/{eventName}/{auditoriumName}"/*, headers = "Accept=application/pdf"*/)
    public ModelAndView bookingTicketListPdfFormat(@PathVariable String eventName, @PathVariable String auditoriumName) {
        Map<String, Object> model = new HashMap<>();
        List<Ticket> tickets = bookingService.getTicketsForEvent(eventName, auditoriumName,
                LocalDateTime.of(2011, Month.APRIL, 20, 10, 10));
        model.put("tickets", tickets);
        return new ModelAndView(new TicketPdfView(), model);
    }

}
