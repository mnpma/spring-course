package com.hm.springcourse.controller;

import com.hm.springcourse.dto.AuditoriumDto;
import com.hm.springcourse.model.Auditorium;
import com.hm.springcourse.service.AuditoriumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class AuditoriumController {
    @Autowired
    private AuditoriumService auditoriumService;
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/auditorium/{name}")
    public String getAuditorium(@PathVariable(name="name")String name, Model model) {
        Auditorium auditorium = auditoriumService.getByName(name);
        model.addAttribute("auditorium", auditorium);
        return "auditoriumView";
    }

    @GetMapping("/auditoriumList")
    public String getAuditoriumList(Model model) {
        List<Auditorium> auditoriums = auditoriumService.getAll();
        model.addAttribute("auditoriums", auditoriums);
        return "auditoriumList";
    }

    @RequestMapping(value = { "/addAuditorium" }, method = RequestMethod.GET)
    public String addAuditoriumDto(Model model) {
        AuditoriumDto auditoriumDto = new AuditoriumDto();
        model.addAttribute("auditoriumDto", auditoriumDto);
        return "addAuditorium";
    }

    @RequestMapping(value = { "/addAuditorium" }, method = RequestMethod.POST)
    public String save(Model model,
                                @ModelAttribute("auditoriumDto") AuditoriumDto auditoriumDto) {
        String name = auditoriumDto.getName();
        String seatsNumber = auditoriumDto.getSeatsNumber();
        String vipSeats = auditoriumDto.getVipSeats();
        auditoriumService.save(name, seatsNumber, vipSeats);
        return "redirect:/auditoriumList";
    }
}
