package com.hm.springcourse.controller;

import com.hm.springcourse.model.Event;
import com.hm.springcourse.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class EventController {
    @Autowired
    private EventService eventService;
    @GetMapping("/eventList")
    public String getAll(Model model) {
        List<Event> events = eventService.getAll();
        model.addAttribute("events", events);
        return "eventList";
    }
    @GetMapping("/event/name/{name}")
    public String getEventByName(@PathVariable String name, Model model) {
        List<Event> events = eventService.getByName(name);
        model.addAttribute("events", events);
        return "eventList";
    }

    @GetMapping("/event")
    public String getEventByNameDateAuditoriumId(Model model,
                                                 @RequestParam("date")String localDateTime,
                                                 @RequestParam("name")String name,
                                                 @RequestParam("auditoriumId")Long auditoriumId) {
        LocalDateTime ldt = LocalDateTime.parse(localDateTime, DateTimeFormatter.ISO_DATE_TIME);
        ///event/?name=Event1&date=2011-04-20T10:10&auditoriumId=1
        Event event = eventService.getEvent(name, auditoriumId, ldt);
        model.addAttribute("event", event);
        return "eventView";
    }

    @PostMapping("/eventUpload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file) throws Exception {
        eventService.saveJsonObject(file);
        return "redirect:/eventList";
    }
}
