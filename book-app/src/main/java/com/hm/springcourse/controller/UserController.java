package com.hm.springcourse.controller;

import com.hm.springcourse.dto.UserDto;
import com.hm.springcourse.model.User;
import com.hm.springcourse.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    public String getUser(@PathVariable Long id, Model model) {
        User user = userService.getById(id);
        model.addAttribute("user", user);
        return "userView";
    }

    @GetMapping("/user/email/{email}")
    public String getUserByEmail(@PathVariable String email, Model model) {
        User user = userService.getUserByEmail(email);
        model.addAttribute("user", user);
        return "userView";
    }

    @GetMapping("/userList")
    public String getAllUser(Model model) {
        //List<User> users = userService.findAll();
        List<UserDto> users = userService.findUserDtoAll();
        model.addAttribute("users", users);
        return "userList";
    }

    @GetMapping("/userListSummary")
    public String getSummary(Model model) {
        List<UserDto> users = userService.findUserDtoAll();
        model.addAttribute("userDtos", users);
        return "userListSummary";
    }

    @GetMapping("/user/name/{name}")
    public String getUsers(@PathVariable String name, Model model) {
        List<User> users = userService.getUsersByName(name);
        model.addAttribute("users", users);
        return "userList";
    }

    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file) throws Exception {
        userService.saveJsonObject(file);
        return "redirect:/userList";
    }

/*
    @GetMapping("/ticketList")
    public String getBookedTickets(Model model) {
        List<Ticket> tickets = userService.getBookedTickets();
        model.addAttribute("tickets", tickets);
        return "ticketList";
    }

*/
}
