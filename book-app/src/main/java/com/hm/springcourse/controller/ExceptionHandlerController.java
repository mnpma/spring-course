package com.hm.springcourse.controller;

import com.hm.springcourse.exception.BookException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerController {
    @ExceptionHandler(BookException.class)
    public ModelAndView handleCustomException(BookException ex) {
        ModelAndView model = new ModelAndView("genericError");
        model.addObject("errMsg", ex.getMessage());
        return model;

    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleAllException(Exception ex) {
        ModelAndView model = new ModelAndView("genericError");
        model.addObject("errMsg", ex.getMessage());
        return model;
    }

}
