package com.hm.springcourse;

import com.hm.springcourse.dao.AuditoriumRepository;
import com.hm.springcourse.dao.EventRepository;
import com.hm.springcourse.dao.UserRepository;
import com.hm.springcourse.model.Auditorium;
import com.hm.springcourse.model.Event;
import com.hm.springcourse.model.Rate;
import com.hm.springcourse.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;


@SpringBootApplication(scanBasePackages = {"com.hm"})
public class BookApplication {
	private static final Logger log = LoggerFactory.getLogger(BookApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(BookApplication.class, args);
	}

	@Bean
	public CommandLineRunner generateUser(UserRepository userRepository
            , AuditoriumRepository auditoriumRepository, EventRepository eventRepository) {
		return args -> {
            String email = "user.test@gmail.com";
            String userName = "User Test";
            userRepository.save(new User(email, userName, LocalDate.now()));

            Auditorium auditorium = auditoriumRepository.getOne(1l);
            Event event = new Event("Event1", Rate.HIGH, 123.456, LocalDateTime.of(2011, Month.APRIL, 20, 10, 10));
            event.setAuditorium(auditorium);
            event = eventRepository.save(event);

		};
	}
}
