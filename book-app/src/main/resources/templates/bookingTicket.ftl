<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Booking Ticket</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
      <#if errorMessage??>
      <div style="color:red;font-style:italic;">
          ${errorMessage}
      </div>
      </#if>

<div>
    <fieldset>
        <legend>Booking Ticket</legend>
        <form name="bookingTicket" action="" method="POST">
            Event Name: <@spring.formInput "bookingTicketDto.eventName" "" "text"/>    <br/>
            Auditorium Name: <@spring.formInput "bookingTicketDto.auditoriumName" "[Enter Auditorial Name]" "text"/>    <br/>
            Seats: <@spring.formInput "bookingTicketDto.seats" "" "text"/>    <br/>
            Event Date: <@spring.formInput "bookingTicketDto.eventDateTime" "" "text"/>    <br/>
            User: <@spring.formSingleSelect path="bookingTicketDto.email" options=userMapDto/>    <br/>
            Price: ${bookingTicketDto.ticketPrice} <br/>
            <input type="submit" value="Create" />
        </form>
    </fieldset>
</div>


</body>

</html>