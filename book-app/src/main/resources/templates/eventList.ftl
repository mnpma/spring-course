<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Event List</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>Event List</h3>
<a href="index">Index</a>
<br><br>
<div>
    <form name="eventList" action="/eventUpload" method="POST" enctype="multipart/form-data">
    <table border="1">
        <tr>
            <th>Name</th>
            <th>Rate</th>
            <th>BasePrice</th>
            <th>Date</th>
<#--
            <th>Auditorium Name</th>
-->
            <th>GoTo View</th>
        </tr>
            <#list events as event>
            <tr>
                <td><a href="<@spring.url '/event/name/'/>${event.name}">${event.name}</a></td>
                <td>${event.rate}</td>
                <td>${event.basePrice}</td>
                <td>${event.dateTime}</td>
<#--
                <td><a href="<@spring.url '/auditorium/'/>${event.auditorium.name}">${event.auditorium.name}</a></td>
-->
                <td><a href="<@spring.url '/event/'/>?name=${event.name}&date=${event.dateTime}&auditoriumId=${event.auditorium.id}">GoToView</a></td>
            </tr>
            </#list>
    </table>
    <input name="file" type="file" />
    <input type="submit" value="Submit" />
    </form>
</div>
</body>
</html>