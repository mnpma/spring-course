<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>User List</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>User List</h3>
<a href="addUser">Add User</a>
<br><br>
<div>
    <form name="userList" action="/upload" method="POST" enctype="multipart/form-data">
    <table border="1">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>BirthDay</th>
            <th>####</th>
            <th>Event Name</th>
            <th>Price</th>
        </tr>
            <#list users as user>
            <tr>
                <td><a href="<@spring.url '/user/'/>${user.id}">${user.id}</a></td>
                <td><a href="<@spring.url '/user/name/'/>${user.name}">${user.name}</a></td>
                <td><a href="<@spring.url '/user/email/'/>${user.email}">${user.email}</a></td>
                <td>${user.birthday}</td>
                <td><a href="<@spring.url '/bookingTicket?'/>userId=${user.id}">Booking Ticket - Calculate Price</a></td>
                <td>${user.eventName}</td>
                <td>${user.price}</td>
            </tr>
            </#list>
    </table>
        <input name="file" type="file" />
        <input type="submit" value="Submit" />
    </form>
</div>
</body>
</html>