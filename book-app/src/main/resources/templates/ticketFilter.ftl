<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Ticket Filter</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<a href="<@spring.url '/index'/>">Index</a>
<div>
    <fieldset>
        <legend>Ticket Filter</legend>
        <form name="ticketFilter" action="" method="POST">
            Event Name:  <@spring.formSingleSelect path="ticketFilterDto.eventName" options=eventNameList/>    <br/>

            Auditorium Name:  <@spring.formSingleSelect path="ticketFilterDto.auditoriumName" options=auditoriumNameList/>    <br/>

            Report Type:  <@spring.formSingleSelect path="ticketFilterDto.reportType" options=reportTypeList/>    <br/>

            <input type="submit" value="Create" />
        </form>
    </fieldset>
</div>


</body>

</html>