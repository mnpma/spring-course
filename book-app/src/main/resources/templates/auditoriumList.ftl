<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Auditorium List</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>Auditorium List</h3>
<a href="addAuditorium">Add Auditorium</a>
<br><br>
<div>

    <table border="1">
        <tr>
            <th>Name</th>
            <th>Seats</th>
            <th>Vip Seats</th>
        </tr>
            <#list auditoriums as auditorium>
            <tr>
                <td><a href="<@spring.url '/auditorium/'/>${auditorium.name}">${auditorium.name}</a></td>
                <td>${auditorium.seatsNumber}</td>
                <td>${auditorium.vipSeats}</td>
            </tr>
            </#list>
    </table>
</div>
</body>
</html>