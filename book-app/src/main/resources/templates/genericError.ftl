<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Error View</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>ERROR</h3>

<br/><br/>
<a href="<@spring.url '/index'/>">Index</a>
<br/>
<div>
    <p> MESSAGE : ${errMsg}</p>
    <br/>
</div>
</body>
</html>