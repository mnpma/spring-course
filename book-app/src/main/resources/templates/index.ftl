<#import "/spring.ftl" as spring/>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Book-App</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>

<body>

<a href="<@spring.url '/auditoriumList'/>">Auditorium List</a> <br/>

<a href="<@spring.url '/userList'/>">User List</a><br/>

<a href="<@spring.url '/eventList'/>">Event List</a><br/>

<a href="<@spring.url '/ticketFilter'/>">Ticket Filter</a>

</body>

</html>