<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Auditorium View</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>Auditorium view</h3>

<br/><br/>
<a href="<@spring.url '/auditoriumList'/>">Auditorium List</a>
<br/>
<div>
    <p> Name: ${auditorium.name}</p>
    <br/>
    <p>Seats: ${auditorium.seatsNumber}</p>
    <br/>
    <p>Vip Seats: ${auditorium.vipSeats}</p>
    <br/>
</div>
</body>
</html>