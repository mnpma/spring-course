<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Add Auditorium</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
      <#if errorMessage??>
      <div style="color:red;font-style:italic;">
          ${errorMessage}
      </div>
      </#if>

<div>
    <fieldset>
        <legend>Add Auditorium</legend>
        <form name="auditorium" action="" method="POST">
            Name: <@spring.formInput "auditoriumDto.Name" "" "text"/>    <br/>
            Seats Number: <@spring.formInput "auditoriumDto.seatsNumber" "" "text"/>    <br/>
            VIP Seats: <@spring.formInput "auditoriumDto.vipSeats" "" "text"/>    <br/>
            <input type="submit" value="Create" />
        </form>
    </fieldset>
</div>


</body>

</html>