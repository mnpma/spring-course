<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Booked Ticket List</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>Ticket List</h3>
<a href="<@spring.url '/index'/>">Index</a>
<br><br>
<div>

    <table border="1">
        <tr>
            <th>Id</th>
            <th>Date</th>
            <th>Seats</th>
            <th>Price</th>
        </tr>
            <#list tickets as ticket>
            <tr>
                <td><a href="<@spring.url '/ticket/'/>${ticket.id}">${ticket.id}</a></td>
                <td>${ticket.dateTime}</td>
                <td>${ticket.seats}</td>
                <td>${ticket.price}</td>
            </tr>
            </#list>
    </table>
</div>
</body>
</html>