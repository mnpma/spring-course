<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>Event View</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>Event view</h3>

<br/><br/>
<a href="<@spring.url '/eventList'/>">Event List</a>
<br/>
<div>
    <p> Name: ${event.name}</p>
    <br/>
    <p>Rate: ${event.rate}</p>
    <br/>
    <p>BasePrice: ${event.basePrice}</p>
    <br/>
</div>
</body>
</html>