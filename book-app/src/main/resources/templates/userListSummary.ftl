<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>User List</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>User List Summary</h3>
<a href="<@spring.url '/userList'/>">User List</a>
<br><br>
<div>

    <table border="1">
        <tr>

            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>BirthDay</th>
            <th>Event Name</th>
            <th>Price</th>
        </tr>
            <#list userDtos as user>
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.email}</td>
                <td>${user.birthday}</td>
                <td>${user.eventName}</td>
                <td>${user.price}</td>
            </tr>
            </#list>
    </table>
</div>
</body>
</html>