<#import "/spring.ftl" as spring/>

<html>
<head>
    <title>User View</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>User view</h3>

<br/><br/>
<a href="<@spring.url '/userList'/>">User List</a>
<br/>
<div>
    <p> Name: ${user.name}</p>
    <br/>
    <p>Email: ${user.email}</p>
    <br/>
    <p>BirthDay: ${user.birthday}</p>
    <br/>
</div>
</body>
</html>