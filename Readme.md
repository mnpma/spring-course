### Booking Service

#### Build and Run tests:
```
cd spring-course
mvnw clean install
```

#### Run
```
cd spring-course
mvnw spring-boot:run -pl book-app
```
#### Run jar
```
java -jar target/book-app-0.0.1-SNAPSHOT.jar
```
#### Health Check
```
http://localhost:8080/ping
```
###### H2 console
```
http://localhost:8080/h2-console/
the default jdbc url : jdbc:h2:mem:testdb
```

##### URL
http://localhost:8080/

#### Model
```
Auditorium contains information about seats, Auditorium's name, vipSeats
User contains information about User(email, name, birthdate) 
Event contains information about Event's name, rate, basePrice, Event's date and what Auditoriums belong to Event
Ticket contains info on which Event, User it is, Date(when ticket is bought), seats, price
Booking - TicketId, UserId 
```
##### MODULE 1
```
2. For all Booking operations implement Spring MVC annotation-based controllers.

3. For operations that return one or several entites as a result (e.g. getUserByEmail, getUsersByName, getBookedTickets) 
implement simple views rendered via Freemarker template engine. Use FreeMarkerViewResolver for view resolving procedure.

4. For operations, that return list of booked tickets (by event, or by user) implement alternative controllers, that will return result as PDF document. Map this controller to a specific value of Accept request header  - Accept=application/pdf

5. Implement batch loading of users and events into system. In order to do this, create controller which accepts multipart file upload, parses it and calls all Booking functionality methods to add events and users into the system. The format of the file (JSON, XML, ...) is up to you as long as you can implement the correct parsing procedure.

6. Implement generic exception handler which should redirect all controller exceptions to simple Freemarker view, that just prints exception message.
```

#### Notes to 4. implement alternative controllers, that will return result as PDF document. Map this controller to a specific value of Accept request header  - Accept=application/pdf
[iText moved from the MPLicense to the AGPLicense](http://mvnrepository.com/artifact/com.lowagie/itext/4.2.2)
```
After release 2.1.7, iText moved from the MPLicense to the AGPLicense. The groupId changed from com.lowagie to com.itextpdf and the artifactId from itext to itextpdf. See http://itextpdf.com/functionalitycomparison for more information.
```

#### REST Controllers
User
[/restUserList](http://localhost:8080/restUserList)
Result is
```
[{"id":1,"email":"user.test@gmail.com","name":"User Test","birthday":"2018-09-03","tickets":[],"bookings":[]}]
```
Event
[/restEventList](http://localhost:8080/restEventList)
Result is
```
[{"id":1,"name":"Event1","rate":"HIGH","basePrice":123.456,"dateTime":"2011-04-20T10:10:00","auditorium":{"id":1,"name":"Blue hall","seatsNumber":500,"vipSeats":"25,26,27,28,29,30,31,32,33,34,35","events":[],"vipSeatsList":[25,26,27,28,29,30,31,32,33,34,35]},"tickets":[]}]
```
#### Below Issue was fixed by adding method: "public void setBirthday(String birthday)" to User Entity
com.fasterxml.jackson.databind.exc.InvalidDefinitionException: Cannot construct instance of `java.time.LocalDate` (no Creators, like default construct, exist): no String-argument constructor/factory method to deserialize from String value ('2008-09-03T10:10')