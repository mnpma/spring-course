package com.hm.springcourse.util;

import com.hm.springcourse.exception.BookException;

public final class Check {
    public static void notNull(Object object, String message, Object... args) {
        if (object == null) {
            throw new BookException(message, args);
        }
    }

}
