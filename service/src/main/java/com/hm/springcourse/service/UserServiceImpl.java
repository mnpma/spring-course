package com.hm.springcourse.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hm.springcourse.dao.UserCustomRepository;
import com.hm.springcourse.dao.UserRepository;
import com.hm.springcourse.dto.UserDto;
import com.hm.springcourse.model.Ticket;
import com.hm.springcourse.model.User;
import com.hm.springcourse.util.Check;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import java.util.stream.Collectors;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private UserCustomRepository userCustomRepository;
    @Autowired
    private FileUploadService fileUploadService;

    @Override
    public User register(User user) {
        return repository.save(user);
    }

    @Override
    public void remove(User user) {
        repository.delete(user);
    }

    @Override
    public User getById(long id) {
        Optional<User> userOptional = repository.findById(Long.valueOf(id));
        Check.notNull(userOptional.isPresent() ? userOptional.get() : null, "exception.notFound",
                "Id", id);
        return userOptional.get();
    }

    @Override
    public User getUserByEmail(String email) {
        Optional<User> userOptional = repository.findByEmail(email);
        Check.notNull(userOptional.isPresent() ? userOptional.get() : null, "exception.notFound",
                "email", email);
        return userOptional.get();
    }

    @Override
    public List<User> getUsersByName(String name) {
        Optional<List<User>> userOptional = repository.findByName(name);
        Check.notNull(userOptional.isPresent() ? userOptional.get() : null, "exception.notFound",
                "name", name);
        return userOptional.get();
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Ticket> getBookedTickets() {
        List<Ticket> tickets = new ArrayList<>();//repository.getBookedTickets();
        Check.notNull(tickets != null ? tickets : null, "exception.notFound",
                "booked tickets", "");
        return tickets;
    }

    /*
        @Override
        public Map<Long, UserDto>  getUserListDto() {
            Map<Long, UserDto> result = findAll().stream().map((p)->new UserDto(p.getId(), p.getEmail()))
                    .collect(Collectors.toMap(UserDto::getId, Function.identity()));

            return result;
        }
    */
    @Override
    public List<String> getUserEmailListDto() {
        return findAll().stream().map(User::getEmail).collect(Collectors.toList());
    }

    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Override
    public List<UserDto> findUserDtoAll() {
        List<UserDto> userDtoList = userCustomRepository.findUserDtoAll();
        if (userDtoList.size() == 0) {
            List<User> users = repository.findAll();
            for (User user : users) {
                userDtoList.add(new UserDto(user.getId(), user.getEmail(), user.getName(),
                        user.getBirthday(), "", 0.0));
            }
        }
        return userDtoList;
    }

    public void saveJsonObject(MultipartFile file) throws Exception {
        Path path = fileUploadService.handleFileUpload(file);
        ObjectMapper objectMapper = new ObjectMapper();
        List<User> users = objectMapper.readValue(new File(path.toString()), new TypeReference<List<User>>() {
        });
        for(User user : users) {
            repository.save(user);
        }
    }
}
