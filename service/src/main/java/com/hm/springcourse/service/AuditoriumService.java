package com.hm.springcourse.service;

import com.hm.springcourse.model.Auditorium;

import java.util.List;

public interface AuditoriumService {
    List<Auditorium> getAll();

    Auditorium getByName(String name);

    int getSeatsNumber(String auditoriumName);

    List<Integer> getVipSeats(String auditoriumName);
    Auditorium save(String name, String seatsNumber, String vipSeats);
}
