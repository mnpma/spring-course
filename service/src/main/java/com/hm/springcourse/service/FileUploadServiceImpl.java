package com.hm.springcourse.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service("fileUploadService")
public class FileUploadServiceImpl implements FileUploadService{
    @Override
    public Path handleFileUpload(MultipartFile file) throws Exception {
        // Get the file and save it somewhere
        byte[] bytes = file.getBytes();
        File temp = File.createTempFile(file.getOriginalFilename(), "");
        Path path = Paths.get(temp.getPath());
        Files.write(path, bytes);
        return path;
    }
}
