package com.hm.springcourse.service;

import com.hm.springcourse.dao.AuditoriumRepository;
import com.hm.springcourse.model.Auditorium;
import com.hm.springcourse.util.Check;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service("auditoriumService")
@Transactional
public class AuditoriumServiceImpl implements AuditoriumService {
    private final Logger log = LoggerFactory.getLogger(AuditoriumServiceImpl.class);
    @Autowired
    private AuditoriumRepository repository;
    @Override
    public List<Auditorium> getAll() {
        log.debug("Return All Auditoriums");
        return repository.findAll();
    }

    @Override
    public Auditorium getByName(String name) {
        log.debug("Find Auditorium byName {}", name);
        Optional<Auditorium> auditoriumOptional = repository.findByName(name);
        Check.notNull(auditoriumOptional.isPresent() ? auditoriumOptional.get() : null, "exception.notFound",
                "name", name);
        return auditoriumOptional.get();
    }

    @Override
    public int getSeatsNumber(String auditoriumName) {
        return getByName(auditoriumName).getSeatsNumber();
    }

    @Override
    public List<Integer> getVipSeats(String auditoriumName) {
        return getByName(auditoriumName).getVipSeatsList();
    }

    @Override
    public Auditorium save(String name, String seatsNumber, String vipSeats) {
        int seatsNum = Integer.parseInt(seatsNumber);
        return repository.save(new Auditorium(name, seatsNum, vipSeats));
    }
}
