package com.hm.springcourse.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hm.springcourse.dao.EventCustomRepository;
import com.hm.springcourse.dao.EventRepository;
import com.hm.springcourse.model.Auditorium;
import com.hm.springcourse.model.Event;
import com.hm.springcourse.util.Check;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service("eventService")
@Transactional
public class EventServiceImpl implements EventService{
    @Autowired
    private EventRepository repository;
    @Autowired
    private EventCustomRepository eventCustomRepository;
    @Autowired
    private FileUploadService fileUploadService;
    @Override
    public Event create(Event event) {
        return repository.save(event);
    }

    @Override
    public void remove(Event event) {
        repository.delete(event);
    }

    @Override
    public Event getEvent(String name, /*Auditorium auditorium*/Long auditoriumId, LocalDateTime dateTime) {
        Optional<Event> eventOptional = eventCustomRepository.findByNameDateTimeAuditoriumId(name, auditoriumId, dateTime);
        Check.notNull(eventOptional.isPresent() ? eventOptional.get() : null, "exception.notFound",
                "name", name);
        return eventOptional.get();
    }

    @Override
    public List<Event> getByName(String name) {
        Optional<List<Event>> eventOptional = repository.findByName(name);
        Check.notNull(eventOptional.isPresent() ? eventOptional.get() : null, "exception.notFound",
                "name", name);
        return eventOptional.get();
    }

    @Override
    public List<Event> getAll() {
        return repository.findAll();
    }

    @Override
    public List<Event> getForDateRange(LocalDateTime from, LocalDateTime to) {
        return null;
    }

    @Override
    public List<Event> getNextEvents(LocalDateTime to) {
        return null;
    }

    @Override
    public Event assignAuditorium(Event event, Auditorium auditorium, LocalDateTime date) {
        return null;
    }

    @Override
    public void saveJsonObject(MultipartFile file) throws Exception {
        Path path = fileUploadService.handleFileUpload(file);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Event> events = objectMapper.readValue(new File(path.toString()), new TypeReference<List<Event>>() {
        });
        for(Event event : events) {
            repository.save(event);
        }
    }
}
