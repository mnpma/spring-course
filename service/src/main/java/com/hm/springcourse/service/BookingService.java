package com.hm.springcourse.service;

import com.hm.springcourse.model.Ticket;
import com.hm.springcourse.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface BookingService {

    Ticket getTicketWithPrice(String eventName, String auditoriumName, LocalDateTime eventDateTime, List<Integer> seats, Long userId);

    Ticket bookTicket(User user, Ticket ticket);

    List<Ticket> getTicketsForEvent(String eventName, String auditoriumName, LocalDateTime date);
}
