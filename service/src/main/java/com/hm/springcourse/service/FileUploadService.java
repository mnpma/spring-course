package com.hm.springcourse.service;

import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

public interface FileUploadService {
    Path handleFileUpload(MultipartFile file)throws Exception;
}
