package com.hm.springcourse.service;

import com.hm.springcourse.dao.BookingCustomRepository;
import com.hm.springcourse.dao.TicketRepository;
import com.hm.springcourse.model.*;
import com.hm.springcourse.util.Check;
import com.hm.springcourse.util.CsvUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("bookingService")
@PropertySource({"classpath:strategies/booking.properties"})
@Transactional
public class BookingServiceImpl implements BookingService {
    final         int               minSeatNumber;
    final         double            vipSeatPriceMultiplier;
    final         double            highRatedPriceMultiplier;
    final         double            defaultRateMultiplier;

    @Autowired
    private AuditoriumService auditoriumService;
    @Autowired
    private EventService eventService;
    @Autowired
    private UserService userService;
    @Autowired
    private BookingCustomRepository bookingCustomRepository;
    @Autowired
    private TicketRepository ticketRepository;


    public BookingServiceImpl(@Value("${min.seat.number}") int minSeatNumber,
                                                            @Value("${vip.seat.price.multiplier}") double vipSeatPriceMultiplier,
                                                            @Value("${high.rate.price.multiplier}") double highRatedPriceMultiplier,
                                                            @Value("${def.rate.price.multiplier}") double defaultRateMultiplier) {
        this.minSeatNumber = minSeatNumber;
        this.vipSeatPriceMultiplier = vipSeatPriceMultiplier;
        this.highRatedPriceMultiplier = highRatedPriceMultiplier;
        this.defaultRateMultiplier = defaultRateMultiplier;
    }

    @Override
    public Ticket getTicketWithPrice(String eventName, String auditoriumName, LocalDateTime eventDateTime,
                                 List<Integer> seats, Long userId) {
        if (Objects.isNull(eventName)) {
            throw new NullPointerException("Event name is [null]");
        }
        if (Objects.isNull(seats)) {
            throw new NullPointerException("Seats are [null]");
        }
        if (seats.contains(null)) {
            throw new NullPointerException("Seats contain [null]");
        }
        final Auditorium auditorium = auditoriumService.getByName(auditoriumName);
        User user = userService.getById(userId);
        if (Objects.isNull(user)) {
            throw new NullPointerException("User is [null]");
        }

        final Event event = eventService.getEvent(eventName, auditorium.getId(), eventDateTime);
        if (Objects.isNull(event)) {
            throw new IllegalStateException(
                    "There is no event with name: [" + eventName + "] in auditorium: [" + auditorium + "] on date: ["
                            + eventDateTime + "]");
        }

        final double baseSeatPrice = event.getBasePrice();
        final double rateMultiplier = event.getRate() == Rate.HIGH ? highRatedPriceMultiplier : defaultRateMultiplier;
        final double seatPrice = baseSeatPrice * rateMultiplier;
        final double vipSeatPrice = vipSeatPriceMultiplier * seatPrice;
        //final double discount = discountService.getDiscount(user, event);
        validateSeats(seats, auditorium);

        final List<Integer> auditoriumVipSeats = auditorium.getVipSeatsList();
        final List<Integer> vipSeats = auditoriumVipSeats.stream().filter(seats:: contains).collect(
                Collectors.toList());
        final List<Integer> simpleSeats = seats.stream().filter(seat -> !vipSeats.contains(seat)).collect(
                Collectors.toList());

        final double simpleSeatsPrice = simpleSeats.size() * seatPrice;
        final double vipSeatsPrice = vipSeats.size() * vipSeatPrice;

        //        System.out.println("auditoriumVipSeats = " + auditoriumVipSeats);
        //        System.out.println("baseSeatPrice = " + baseSeatPrice);
        //        System.out.println("rateMultiplier = " + rateMultiplier);
        //        System.out.println("vipSeatPriceMultiplier = " + vipSeatPriceMultiplier);
        //        System.out.println("seatPrice = " + seatPrice);
        //        System.out.println("vipSeatPrice = " + vipSeatPrice);
        //        System.out.println("discount = " + discount);
        //        System.out.println("seats = " + seats);
        //        System.out.println("simpleSeats.size() = " + simpleSeats.size());
        //        System.out.println("vipSeats.size() = " + vipSeats.size());

        final double totalPrice = simpleSeatsPrice + vipSeatsPrice;
        String seatsString = CsvUtil.fromListToCsv(seats);
        Ticket ticket = new Ticket(LocalDateTime.of(2011, Month.APRIL, 20, 10, 10),
                seatsString, totalPrice);
        ticket.setEvent(event);
        ticket.setUser(user);
        Ticket storedTicket = ticketRepository.save(ticket);
        user.getTickets().add(storedTicket);

        userService.save(user);

        return storedTicket;
    }

    private void validateSeats(List<Integer> seats, Auditorium auditorium) {
        final int seatsNumber = auditorium.getSeatsNumber();
        final Optional<Integer> incorrectSeat = seats.stream().filter(
                seat -> seat < minSeatNumber || seat > seatsNumber).findFirst();
        incorrectSeat.ifPresent(seat -> {
            throw new IllegalArgumentException(
                    String.format("Seat: [%s] is incorrect. Auditorium: [%s] has [%s] seats", seat, auditorium.getName(),
                            seatsNumber));
        });
    }

    @Override
    public Ticket bookTicket(User user, Ticket ticket) {
        if (Objects.isNull(user)) {
            throw new NullPointerException("User is [null]");
        }
        User foundUser = userService.getById(user.getId());
        if (Objects.isNull(foundUser)) {
            throw new IllegalStateException("User: [" + user + "] is not registered");
        }

        Optional<List<Ticket>> bookedTicketsOp = bookingCustomRepository.getTickets(ticket.getEvent().getId());
        Check.notNull(bookedTicketsOp.isPresent() ? bookedTicketsOp.get():null, "exception.notFound", "ticket.getEvent().getId()", ticket.getEvent().getId());
        List<Ticket> bookedTickets = bookedTicketsOp.get();
        boolean seatsAreAlreadyBooked = bookedTickets.stream().filter(bookedTicket -> ticket.getSeatsList().stream().filter(
                bookedTicket.getSeatsList() :: contains).findAny().isPresent()).findAny().isPresent();

        if (!seatsAreAlreadyBooked)
            bookingCustomRepository.create(user, ticket);
        else
            throw new IllegalStateException("Unable to book ticket: [" + ticket + "]. Seats are already booked.");
        return ticket;
    }

    @Override
    public List<Ticket> getTicketsForEvent(String eventName, String auditoriumName, LocalDateTime date) {
        final Auditorium auditorium = auditoriumService.getByName(auditoriumName);
        //Event getEvent(String name, Long auditoriumId, LocalDateTime dateTime);
        final Event foundEvent = eventService.getEvent(eventName, auditorium.getId(), date);
        Optional<List<Ticket>> bookedTicketsOp = bookingCustomRepository.getTickets(foundEvent.getId());
        Check.notNull(bookedTicketsOp.isPresent() ? bookedTicketsOp.get():null, "exception.notFound",
                "foundEvent.getId", foundEvent.getId());
        return bookedTicketsOp.get();
    }
}
