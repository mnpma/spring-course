package com.hm.springcourse.service;

import com.hm.springcourse.dto.UserDto;
import com.hm.springcourse.model.Ticket;
import com.hm.springcourse.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

public interface UserService {

    User register(User user);

    void remove(User user);

    User getById(long id);

    User getUserByEmail(String email);

    List<User> getUsersByName(String name);
    List<User> findAll();

    List<Ticket> getBookedTickets();
    List<String>  getUserEmailListDto();
    User save(User user);

    List<UserDto> findUserDtoAll();
    void saveJsonObject(MultipartFile file) throws Exception;
}
