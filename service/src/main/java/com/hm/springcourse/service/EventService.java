package com.hm.springcourse.service;

import com.hm.springcourse.model.Auditorium;
import com.hm.springcourse.model.Event;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;

public interface EventService {

    Event create(Event event);

    void remove(Event event);

    Event getEvent(String name, Long auditoriumId, LocalDateTime dateTime);

    List<Event> getByName(String name);

    List<Event> getAll();

    List<Event> getForDateRange(LocalDateTime from, LocalDateTime to);

    List<Event> getNextEvents(LocalDateTime to);

    Event assignAuditorium(Event event, Auditorium auditorium, LocalDateTime date);
    void saveJsonObject(MultipartFile file) throws Exception;
}
