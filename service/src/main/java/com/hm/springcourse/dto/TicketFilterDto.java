package com.hm.springcourse.dto;

public class TicketFilterDto {
    private String eventName;
    private String auditoriumName;
    private String reportType;

    public TicketFilterDto() {
    }

    public TicketFilterDto(String eventName, String auditoriumName) {
        this.eventName = eventName;
        this.auditoriumName = auditoriumName;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getAuditoriumName() {
        return auditoriumName;
    }

    public void setAuditoriumName(String auditoriumName) {
        this.auditoriumName = auditoriumName;
    }
}
