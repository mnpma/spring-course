package com.hm.springcourse.dto;

import java.time.LocalDate;

public class UserDto {
    private Long id;
    private String email;
    private String    name;
    private LocalDate birthday;
    private String eventName;
    private Double price;

    protected UserDto() {
    }

    public UserDto(Long id, String email, String name, LocalDate birthday, String eventName, Double price) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.birthday = birthday;
        this.eventName = eventName;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
