package com.hm.springcourse.dao;


import com.hm.springcourse.model.Ticket;
import com.hm.springcourse.model.User;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public interface BookingCustomRepository {
    Optional<List<Ticket>> getTickets(Long eventId);
    Ticket create(User user, Ticket ticket);

    static void validateUser(User user) {
        if (Objects.isNull(user)) {
            throw new NullPointerException("User is [null]");
        }
        if (Objects.isNull(user.getEmail())) {
            throw new NullPointerException("User email is [null]");
        }
    }

    static void validateTicket(Ticket ticket) {
        if (Objects.isNull(ticket)) {
            throw new NullPointerException("Ticket is [null]");
        }
    }
}
