package com.hm.springcourse.dao;

import com.hm.springcourse.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository("userCustomRepository")
public class UserCustomRepositoryImpl implements UserCustomRepository {
    //UserDto(Long id, String email, String name, LocalDate birthday, String eventName, Double price)
    private static final String SELECT_ALL = "select NEW com.hm.springcourse.dto.UserDto(u.id, u.email, u.name, u.birthday, coalesce(ts.event.name, ''), coalesce(ts.price, 0.0)) from User u left join u.tickets ts";
    @Autowired
    EntityManager entityManager;

    @Override
    public List<UserDto> findUserDtoAll() {
        Query q = entityManager.createQuery(SELECT_ALL);
        return q.getResultList();
    }
}
