package com.hm.springcourse.dao;

import com.hm.springcourse.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository("eventCustomRepository")
public class EventCustomRepositoryImpl implements EventCustomRepository {
    private static final String SELECT_BY_NAME_DATE_AUDITORIUM_ID = "select ev from Event ev join ev.auditorium a where ev.dateTime =:dateTime and ev.name =:name and a.id =:auditoriumId";
    @Autowired
    EntityManager entityManager;
    @Override
    public Optional<Event> findByNameDateTimeAuditoriumId(String name, Long auditoriumId, LocalDateTime dateTime) {
        Query q = entityManager.createQuery(SELECT_BY_NAME_DATE_AUDITORIUM_ID);
        q.setParameter("dateTime", dateTime);
        q.setParameter("name", name);
        q.setParameter("auditoriumId", auditoriumId);
        List<Event> events = q.getResultList();
        return events == null || events.isEmpty() ? Optional.empty() : Optional.of(events.get(0));
    }
}
