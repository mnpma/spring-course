package com.hm.springcourse.dao;

import com.hm.springcourse.dto.UserDto;


import java.util.List;

public interface UserCustomRepository {

    List<UserDto> findUserDtoAll();
}
