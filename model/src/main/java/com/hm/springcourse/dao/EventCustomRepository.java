package com.hm.springcourse.dao;

import com.hm.springcourse.model.Event;

import java.time.LocalDateTime;
import java.util.Optional;

public interface EventCustomRepository {
    Optional<Event> findByNameDateTimeAuditoriumId(String name, Long auditoriumId, LocalDateTime dateTime);
}
