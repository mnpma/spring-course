package com.hm.springcourse.dao;

import com.hm.springcourse.dto.UserDto;
import com.hm.springcourse.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(@Param("email")String email);
    Optional<List<User>> findByName(@Param("name")String name);
/*
    @Query("select t from User u left join fetch u.bookings bs left join fetch bs.ticket t")
    List<Ticket> getBookedTickets();
*/
}
