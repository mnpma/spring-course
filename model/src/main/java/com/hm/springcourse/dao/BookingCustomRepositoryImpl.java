package com.hm.springcourse.dao;

import com.hm.springcourse.model.Booking;
import com.hm.springcourse.model.Ticket;
import com.hm.springcourse.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository("bookingCustomRepository")
public class BookingCustomRepositoryImpl implements BookingCustomRepository {
    @Autowired
    EntityManager entityManager;
    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private BookingRepository bookingRepository;

    @Override
    public Optional<List<Ticket>> getTickets(Long eventId) {
        Query query = entityManager.createQuery("select b.ticket from Booking b where b.ticket.event.id = :eventId");
        query.setParameter("eventId", eventId);
        List<Ticket> tickets = query.getResultList();
        return Optional.of(tickets);
    }

    @Override
    public Ticket create(User user, Ticket ticket) {
        BookingCustomRepository.validateTicket(ticket);
        BookingCustomRepository.validateUser(user);

        Ticket storedTicket = ticketRepository.save(ticket);
        Long ticketId = (Long) storedTicket.getId();
        Booking booking = new Booking(user, storedTicket);
        bookingRepository.save(booking);
        return storedTicket;
    }
}
