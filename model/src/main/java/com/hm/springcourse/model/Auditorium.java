package com.hm.springcourse.model;

import com.hm.springcourse.util.CsvUtil;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Auditorium {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long   id;
    private String name;
    private int    seatsNumber;
    private String vipSeats;
    @OneToMany
    private Set<Event> events = new HashSet<>();

    public Auditorium() {
    }

    public Auditorium(String name, int seatsNumber, List<Integer> vipSeats) {
        this(name, seatsNumber, CsvUtil.fromListToCsv(vipSeats));
    }

    public Auditorium(String name, int seatsNumber, String vipSeats) {
        this.name = name;
        this.seatsNumber = seatsNumber;
        this.vipSeats = vipSeats;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public String getVipSeats() {
        return vipSeats;
    }

    public List<Integer> getVipSeatsList() {
        return CsvUtil.fromCsvToList(vipSeats, Integer:: valueOf);
    }

    public void setVipSeatsList(List<Integer> vipSeats) {
        this.vipSeats = CsvUtil.fromListToCsv(vipSeats);
    }

    public void setVipSeats(String vipSeats) {
        this.vipSeats = vipSeats;
    }
}
