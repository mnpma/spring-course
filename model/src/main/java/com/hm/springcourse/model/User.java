package com.hm.springcourse.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long      id;
    private String    email;
    private String    name;
    private LocalDate birthday;

    @OneToMany
    private Set<Ticket> tickets = new HashSet<>();
    @OneToMany
    private Set<Booking> bookings = new HashSet<>();

    public Set<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(Set<Booking> bookings) {
        this.bookings = bookings;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    public User() {
    }

    public User(String email, String name, LocalDate birthday) {
        this.email = email;
        this.name = name;
        this.birthday = birthday;
    }

    public User(String email, String name, String birthday) {
        this.email = email;
        this.name = name;
        this.birthday = LocalDate.parse(birthday);
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = LocalDate.parse(birthday);
    }

}
