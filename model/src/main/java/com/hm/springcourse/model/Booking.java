package com.hm.springcourse.model;

import javax.persistence.*;

@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long   id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Ticket ticket;

    protected Booking() {
    }

    public Booking(User user, Ticket ticket) {
        this.user = user;
        this.ticket = ticket;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
